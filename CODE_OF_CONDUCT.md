# Cyanic's Code of Conduct

Version 1.3 from 2025-01-30 by Cyanic76

---

> This release clarifies the Termination Terms.

---

## 1. Definitions

- "I" refers to Cyanic, Cyanic76.
- "You" refers to the reader of this document.
- "The Files" refers to the files contained within a repository.
- "The License file" refers to the license file of the repository.

Some definitions may be added over time.

## 2. Expectations

Nobody, for any given reason, shall be harassed or discriminated through any means, including but not limited to Description, Reactions, Comments and Reviews on Issues and/or Pull Requests.

You shall not gatekeep. You should instead be respectful and welcoming towards everybody else.

Any unacceptable behavior will make you subject to the conditions mentioned in Section 5.

## 3. Reporting Issues

By creating an Issue or a Pull Request, try to make sure that it is not a duplcate of an existing one, including open **and closed**.

You're advised to read [DontAskToAsk](https://dontasktoask.com/) and to know [how to ask a good question](https://stackoverflow.com/help/how-to-ask), but that's not required.

Nobody should blame any fault of a program on the user who reports it and/or their use of it, unless the user proves they misused the said program.

## 4. Asking for new Features

By asking for new features to be added, try to make sure that said feature does not already exist and has not yet been asked for.

## 5. Termination

If any Issue or Pull Request, does not abide by these rules, its author should not, in the future:

- Create an Issue or a Pull Request,
- Publish a Review on a Pull Request,
- Add a Reaction to an Issue or a Pull Request, including Comments,
- Add a Comment to an Issue or a Pull Request.

The above Termination terms do not apply to duplicate and/or invalid issues and/or pull requests.

The above Termination terms are to be considered as a full and immediate ban from all my work, the <a href="./discord">Discord server</a> and the Matrix room.

### A. Discord Termination

If you are banned from my Discord server, you are therefore subject to the above Section 5.

## 6. Repository-specific rules

This document apply to my repositories, only **if and when mentioned**.

Some CODE OF CONDUCT files in other repositories may include specific rules.

Such specific rules can edit but not overwrite this document's Sections 1 to 6 and Sub-Section 5A.

## 7. Edits to this document

This document and documents mentioned in Section 6 may be edited at any time, always with a written announcement.

If a repository-specific CODE OF CONDUCT file is edited, the announcement will only be published on said repository's README file.

---

Resources:

- Contact me at <hi@cyanic.me> with questions or concerns.
