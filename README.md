<div align="center">
  <h1>Hi, this is Cyanic! 👋</h1>

  ---

  [![Matrix](https://img.shields.io/badge/Matrix%20room-a?color=0DBD8B&label=join+the&logoColor=ffffff&style=for-the-badge)](https://r.cyanic.me/?fwd=00400)
  [![Discord](https://img.shields.io/discord/622794903675600926?color=7289da&label=join%20my%20discord%20server&logo=discord&logoColor=ffffff&style=for-the-badge)](https://discord.gg/zp8zF7Zx7y)
  [![Python](https://img.shields.io/badge/-Python-3366ff?style=for-the-badge&logo=python&logoColor=white)](https://github.com/search?q=user%3ACyanic76+language%3APython&type=Repositories&l=Python&l=)
  [![JavaScript](https://img.shields.io/badge/-JavaScript-f7df1e?style=for-the-badge&logo=javascript&logoColor=black)](https://github.com/search?q=user%3ACyanic76+language%3AJavaScript&type=Repositories&l=Python&l=)
  [![NodeJS](https://img.shields.io/badge/-NodeJS-339933?style=for-the-badge&logo=nodejs&logoColor=black)](https://nodejs.org)
  [![HTML & CSS](https://img.shields.io/badge/-HTML%20CSS-e34f26?style=for-the-badge&logo=html5&logoColor=white)](https://cyanic.tk)

  [![Git](https://img.shields.io/badge/-Git%20CLI-ffffff?style=flat-square&logo=git&logoColor=black)](https://git-scm.com/docs/gitcli)
  [![GitHub](https://img.shields.io/badge/-GitHub-000000?style=flat-square&logo=github&logoColor=white)](https://github.com)
  [![Void Linux](https://img.shields.io/badge/-Void%20Linux-39661C?style=flat-square&logo=linux&logoColor=white)](https://void-linux.org/)
  [![PiHole](https://img.shields.io/badge/-PiHole-96060c?style=flat-square&logo=pihole&logoColor=white)](https://pi-hole.net/)
  
  Check out my **[GitHub Gists](https://gist.github.com/Cyanic76)**!

  > My code may be bad or may not work. At least, I write code in a way that it can be read. Unlike [this](https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css) or [this](https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js).
</div>