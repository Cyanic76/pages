async function load() {
  // Get the latest commit timestamp
  fetch("https://codeberg.org/api/v1/repos/Cyanic76/Hosts").then(
    response => response.json()
  ).then(json => {
    const updated = json.uptaded_at.slice(0,10);
    document.getElementById("hosts-data-lastupdate").innerText = updated;
  })
}