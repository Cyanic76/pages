21.0.0
- New look and feel.

20.0.3
- Added `charset=utf-8` to all pages.
- Added `viewport` to all pages.
- Edited colors.
- Edited "Tools" changelog and description.

20.0.2
- Edited "redditdl" references.

20.0.1
- Added ripple to all buttons.
- Added "Less is more" to the homepage & "Work" page footers.
- Moved "redditdl" back to the "Work" subfolder.

20.0.0
- Brand new homepage.
